package com.liuc.algorithm.sort;

/**
 * 1、定义一个学生类Student，具有年龄age和姓名userName两个属性，并通过Comparable接口提供比较规则；
 */
public class Student implements Comparable<Student>{
    private String userName;
    private int age;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Student{" +
                "userName='" + userName + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        return this.getAge()-o.getAge();
    }
}
