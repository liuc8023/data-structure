package com.liuc.algorithm.sort;

public class TestComparable {
    public static Comparable getMax(Comparable c1,Comparable c2){
        int result = c1.compareTo(c2);
        //如果result < 0，则c1比c2小；
        //如果result > 0，则c1比c2大；
        //如果result == 0，则c1和c2一样大；
        if (result >= 0) {
            return c1;
        }else {
            return c2;
        }
    }

    public static void main(String[] args) {
        Student s1 = new Student();
        s1.setAge(20);
        s1.setUserName("张三");
        Student s2 = new Student();
        s2.setAge(21);
        s2.setUserName("李四");
        System.out.println(getMax(s1,s2));
    }
}
