package com.liuc.algorithm.sort.binary;

/**
 * 二分查找
 */
public class BinarySearch {
    /**
     * 二分查找，找到返回元素索引，找不到返回-1
     * @param arr
     * @param t
     * @return
     */
    public static int binarySearch(int[] arr ,int t){
        int l = 0,r = arr.length - 1,m;
        int count = 0;
        while (l<=r){
            //当数组很大时，比如数组长度为Integer.MAX_VALUE时，m =  m = (l + r) / 2;就有可能出现整数溢出问题
//            m = (l + r) / 2;
            m = l + (r -l) / 2;
            count++;
            if (arr[m] == t) {
                System.out.println("总共查找了"+count+"次");
                return m;
            }else if(arr[m] > t){
                r = m -1;
            }else {
                l = m + 1;
            }
        }
        return -1;
    }

    /**
     * 二分查找，找到返回元素索引，找不到返回-1
     * @param arr
     * @param t
     * @return
     */
    public static int binarySearch2(int[] arr ,int t){
        int l = 0,r = arr.length - 1,m;
        int count = 0;
        while (l<=r){
            //当数组很大时，比如数组长度为Integer.MAX_VALUE时，m =  m = (l + r) / 2;就有可能出现整数溢出问题
//            m = (l + r) / 2;
            m = (l + r) >>> 1;
            count++;
            if (arr[m] == t) {
                System.out.println("总共查找了"+count+"次");
                return m;
            }else if(arr[m] > t){
                r = m -1;
            }else {
                l = m + 1;
            }
        }
        return -1;
    }
}
