package com.liuc.algorithm.sort.bubble;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubbleSort {

    /**
     * 对数组a中的元素进行排序
     * 通过flag标识位减少没有意义的比较
     * @param a
     */
    public static void sort(Comparable[] a){
        for (int i = 0; i < a.length-1; i++) {
            //通过flag标识位减少没有意义的比较
            boolean flag=false;
            for (int j = 0; j < a.length-1-i; j++) {
                System.out.println("第"+(j+1)+"次比较");
                //比较索引j和索引j+1处的值
                if (greater(a[j],a[j+1])) {
                    exch(a,j,j+1);
                    flag = true;
                }
            }
            System.out.println("第"+(i+1)+"轮冒泡交换后的结果："+ Arrays.toString(a));
            if (!flag){
                break;
            }
        }
    }

    /**
     * 对数组a中的元素进行排序
     * 优化比较的次数，此方法是最优方式
     * @param a
     */
    public static void sort2(Comparable[] a){
        int n = a.length - 1;
        int count = 0;
        while (true) {
            //表示最后一次交换索引的位置
            int last = 0;
            for (int j = 0; j < n; j++) {
                System.out.println("第" + (j + 1) + "次比较");
                //比较索引j和索引j+1处的值
                if (greater(a[j], a[j + 1])) {
                    exch(a, j, j + 1);
                    last = j;
                }
            }
            n = last;
            count++;
            System.out.println("第"+count+"轮冒泡交换后的结果：" + Arrays.toString(a));
            if (n == 0) {
                break;
            }
        }
    }

    /**
     * 比较v元素是否大于w元素
     * 如果v大于w，则返回true，否则为false
     * @param v
     * @param w
     * @return
     */
    private static boolean greater(Comparable v,Comparable w){
        return v.compareTo(w) > 0;
    }

    /**
     * 数组元素i和j交换位置
     * @param a
     * @param i
     * @param j
     */
    private static void exch (Comparable[] a,int i,int j){
        Comparable temp;
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
