package com.liuc.algorithm.sort.insert;

import java.util.Arrays;

/**
 * 二分插入排序
 */
public class BinaryInsertSort {
    public static void binaryInsertSort(int[] arr){
        long start = System.currentTimeMillis();
        int key, left, right, middle;
        for (int i=1; i<arr.length; i++) {
            key = arr[i];
            left = 0;
            right = i-1;
            while (left<=right) {
                middle = (left+right)/2;
                if (arr[middle]>key) {
                    right = middle-1;
                } else {
                    left = middle+1;
                }
            }
            for(int j=i-1; j>=left; j--) {
                arr[j+1] = arr[j];
            }
            arr[left] = key;
            System.out.println("第"+i+"轮插入排序的结果："+ Arrays.toString(arr));
        }
        long end = System.currentTimeMillis();
        System.out.println("二分插入排序执行花费了"+(end-start)+"ms");
    }
}
