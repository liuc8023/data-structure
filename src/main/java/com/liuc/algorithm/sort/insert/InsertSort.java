package com.liuc.algorithm.sort.insert;

import java.util.Arrays;

/**
 * 插入排序
 * @author liuc
 */
public class InsertSort {
    public static void insertSort(int[] a){
        long start = System.currentTimeMillis();
        //i代表待插入元素的索引
        for (int i = 1; i < a.length; i++) {
            //待插入的元素值
            int temp = a[i];
            //代表已排序区域的元素索引
            int j = i - 1;
            while (j >= 0){
                if (temp < a[j]) {
                    a[j+1] = a[j];
                }else {
                    //退出循环，减少比较的次数
                    break;
                }
                j--;
            }
            a[j+1] = temp;
            System.out.println("第"+i+"轮插入排序的结果："+Arrays.toString(a));
        }
        long end = System.currentTimeMillis();
        System.out.println("插入排序执行花费了"+(end-start)+"ms");
    }
}
