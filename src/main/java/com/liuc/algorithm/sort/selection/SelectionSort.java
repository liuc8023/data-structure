package com.liuc.algorithm.sort.selection;

import java.util.Arrays;

/**
 * 选择排序
 */
public class SelectionSort {
    /**
     * 对数组a中的元素进行排序
     * 4,6,8,7,9,2,10,1
     * @param a
     */
    public static void sort(Comparable[] a){
        System.out.println("原始数据为："+Arrays.toString(a));
        for (int i = 0; i <= a.length -2; i++) {
            //定义一个变量，记录最小元素所在的索引，默认为参与选择排序的第一个元素所在的位置
            int minIndex = i;
            for (int j = i+1; j < a.length; j++) {
                //需要比较最小索引minIndex处的值和j索引处的值的大小
                if (greater(a[minIndex],a[j])) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                exch(a,i,minIndex);
            }
        }
    }

    /**
     * 比较v元素是否大于w元素
     * 如果v大于w，则返回true，否则为false
     * @param v
     * @param w
     * @return
     */
    private static boolean greater(Comparable v,Comparable w){
        return v.compareTo(w) > 0;
    }

    /**
     * 数组元素i和j交换位置
     * @param a
     * @param i
     * @param j
     */
    private static void exch (Comparable[] a,int i,int j){
        Comparable temp;
        temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}
