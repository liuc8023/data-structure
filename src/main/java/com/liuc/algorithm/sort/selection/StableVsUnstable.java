package com.liuc.algorithm.sort.selection;

import java.util.*;

/**
 * 验证冒泡排序与选择排序的稳定性
 * 不稳定排序算法在进行数字排序的时候，会打乱原本同值的花色顺序（原来 ♠2 在前 ♥2 在后，按数字再排后，他俩的位置变了 ）
 * 稳定排序算法按数字排序的时候，会保留原本同值的花色顺序（ ♠2 与 ♥2 的相对位置不变 ）
 */
public class StableVsUnstable {
    public static void main(String[] args) {
        System.out.println("=================选择排序不稳定================");
        Card[] cards = getStaticCards();
        System.out.println(Arrays.toString(cards));
        selection(cards, Comparator.comparingInt((Card a) -> a.sharpOrder).reversed());
        System.out.println(Arrays.toString(cards));
        selection(cards, Comparator.comparingInt((Card a) -> a.numberOrder).reversed());
        System.out.println(Arrays.toString(cards));

        System.out.println("=================冒泡排序稳定=================");
        cards = getStaticCards();
        System.out.println(Arrays.toString(cards));
        bubble(cards, Comparator.comparingInt((Card a) -> a.sharpOrder).reversed());
        System.out.println(Arrays.toString(cards));
        bubble(cards, Comparator.comparingInt((Card a) -> a.numberOrder).reversed());
        System.out.println(Arrays.toString(cards));
    }

    /**
     * 冒泡排序
     * @param a
     * @param comparator
     */
    public static void bubble(Card[] a, Comparator<Card> comparator) {
        int n = a.length - 1;
        while (true) {
            // 表示最后一次交换索引位置
            int last = 0;
            for (int i = 0; i < n; i++) {
                if (comparator.compare(a[i], a[i + 1]) > 0) {
                    swap(a, i, i + 1);
                    last = i;
                }
            }
            n = last;
            if (n == 0) {
                break;
            }
        }
    }

    /**
     * 选择排序
     * @param a
     * @param comparator
     */
    private static void selection(Card[] a, Comparator<Card> comparator) {
        for (int i = 0; i < a.length - 1; i++) {
            // i 代表每轮选择最小元素要交换到的目标索引
            // 代表最小元素的索引
            int s = i;
            for (int j = s + 1; j < a.length; j++) {
                if (comparator.compare(a[s], a[j]) > 0) {
                    s = j;
                }
            }
            if (s != i) {
                swap(a, s, i);
            }
        }
    }

    /**
     * 元素位置交换
     * @param a
     * @param i
     * @param j
     */
    public static void swap(Card[] a, int i, int j) {
        Card t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    enum Sharp {
        diamond, club, heart, spade, black, red
    }
    static Card[] getStaticCards() {
        List<Card> list = new ArrayList<>();
        Card[] copy = Arrays.copyOfRange(Card.cards, 2, 13 * 4 + 2);
        list.add(copy[7]);
        list.add(copy[12]);
        list.add(copy[12+13]);
        list.add(copy[10]);
        list.add(copy[9]);
        list.add(copy[9+13]);
        return list.toArray(new Card[0]);
    }

    static class Card {
        private Sharp sharp;
        private final String number;
        private final int numberOrder;
        private final int sharpOrder;
        static Map<String, Integer> map = new HashMap<>();
        static{
            map.put("RJ", 16);
            map.put("BJ", 15);
            map.put("A", 14);
            map.put("K", 13);
            map.put("Q", 12);
            map.put("J", 11);
            map.put("10", 10);
            map.put("9", 9);
            map.put("8", 8);
            map.put("7", 7);
            map.put("6", 6);
            map.put("5", 5);
            map.put("4", 4);
            map.put("3", 3);
            map.put("2", 2);
        }

        public Card(Sharp sharp, String number) {
            this.sharp = sharp;
            this.number = number;
            this.numberOrder = map.get(number);
            this.sharpOrder = sharp.ordinal();
        }

        private static final Card[] cards;

        static {
            cards = new Card[54];
            Sharp[] sharps = {Sharp.spade, Sharp.heart, Sharp.club, Sharp.diamond};
            String[] numbers = {"A", "K", "Q", "J", "10", "9", "8", "7", "6", "5", "4", "3", "2"};
            int idx = 2;
            for (Sharp sharp : sharps) {
                for (String number : numbers) {
                    cards[idx++] = new Card(sharp, number);
                }
            }
            cards[0] = new Card(Sharp.red, "RJ");
            cards[1] = new Card(Sharp.black, "BJ");
        }

        public String printSharp(Sharp sharp) {
            switch (sharp) {
                case heart : return "[\033[31m" + "♥" + number + "\033[0m]";
                case diamond : return "[\033[31m" + "♦" + number + "\033[0m]";
                case spade : return "[\033[30m" + "♠" + number + "\033[0m]";
                case club : return "[\033[30m" + "♣" + number + "\033[0m]";
                case red : return "[\033[31m" + "\uD83C\uDFAD" + "\033[0m]";
                case black : return "[\033[30m" + "\uD83C\uDFAD" + "\033[0m]";
                default:
                    return "wrong sharp!";
            }
        }

        @Override
        public String toString() {
            return this.printSharp(this.sharp);
        }
    }

}
