package com.liuc.algorithm.sort.shell;

import java.util.Arrays;

/**
 * Hibbard增量序列
 * Hibbard 增量序列的最坏时间复杂度为 O(n^(3/2))；
 * @author liuc
 */
public class HibbardShellSort {
    public static void hibbardShellSort(int[] array){
        System.out.println("初始数组  "+ Arrays.toString(array));
        int count = 0;
        for (int gap = hibbard(array.length); gap > 0 ; gap/=2) {
            System.out.println(gap+"增量");
            for (int i = gap; i < array.length; i++) {
                int j = i;
                int temp = array[i];
                for (; j - gap >= 0&&array[j - gap] > temp;j -= gap) {
                    array[j] = array[j - gap];
                }
                array[j] = temp;
            }
            count++;
            System.out.println("第"+count+"轮希尔排序的结果："+ Arrays.toString(array));
        }
        
    }

    private static int hibbard(int length){
        int i = 1;
        int result;
        for (; i < length; i = 2 * i + 1) {
            ;
        }
        result = (i -1) / 2;
        return result;
    }
}
