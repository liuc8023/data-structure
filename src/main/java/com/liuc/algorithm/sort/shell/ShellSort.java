package com.liuc.algorithm.sort.shell;

import java.util.Arrays;

/**
 * 希尔排序
 * @author liuc
 */
public class ShellSort {
    public static void shellSort(int[] arr){
        //希尔排序的增量
        int d = arr.length;
        int count = 0;
        while (d > 1) {
            //使用希尔增量的方式，即每次折半
            d = d / 2;
            System.out.println(d+"增量");
            for (int x = 0; x < d; x++) {
                for (int i = x + d; i < arr.length; i = i + d) {
                    int temp = arr[i];
                    int j;
                    for (j = i - d; (j >= 0)&&(arr[j] > temp) ; j = j - d) {
                        arr[j + d] = arr[j];
                    }
                    arr[j + d] = temp;
                }
                count++;
                System.out.println("第"+count+"轮希尔排序的结果："+ Arrays.toString(arr));
            }
        }
    }
}
