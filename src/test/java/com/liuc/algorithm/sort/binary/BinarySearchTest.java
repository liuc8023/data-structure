package com.liuc.algorithm.sort.binary;

import org.junit.jupiter.api.Test;

public class BinarySearchTest {
    @Test
    void binarySearch(){
        int[] array = {1,5,8,11,19,22,31,35,40,45,48,49,50};
        int target = 48;
        long start = System.currentTimeMillis();
        int index = BinarySearch.binarySearch(array,target);
        long end = System.currentTimeMillis();
        System.out.println("查找执行时间是："+(end - start)+"ms");
        System.out.println("目标值"+target+"在数组中的位置索引是："+index);
    }

    @Test
    void binarySearch2(){
        int[] array = {1,5,8,11,19,22,31,35,40,45,48,49,50};
        int target = 48;
        long start = System.currentTimeMillis();
        int index = BinarySearch.binarySearch2(array,target);
        long end = System.currentTimeMillis();
        System.out.println("查找执行时间是："+(end - start)+"ms");
        System.out.println("目标值"+target+"在数组中的位置索引是："+index);
    }
}
