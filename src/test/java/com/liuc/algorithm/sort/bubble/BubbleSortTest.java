package com.liuc.algorithm.sort.bubble;

import org.junit.jupiter.api.Test;

public class BubbleSortTest {
    @Test
    void sort(){
        Integer[] a = {4,6,3,5,2,1,9,7,8};
        BubbleSort.sort(a);
    }
    @Test
    void sort2(){
        Integer[] a = {4,6,3,5,2,1,9,7,8};
        BubbleSort.sort2(a);
    }
}
