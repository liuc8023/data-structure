package com.liuc.algorithm.sort.selection;

import org.junit.jupiter.api.Test;
import java.util.Arrays;

/**
 * 选择排序测试类
 */
public class SelectionSortTest {
    @Test
    void sort(){
        //原始数据
        Integer[] a = {4,6,8,7,9,2,10,1};
        SelectionSort.sort(a);
        System.out.println(Arrays.toString(a));
    }
}
