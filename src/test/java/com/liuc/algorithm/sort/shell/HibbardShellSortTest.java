package com.liuc.algorithm.sort.shell;

import org.junit.jupiter.api.Test;

public class HibbardShellSortTest {
    @Test
    void shellSort(){
        int[] arr = {8, 9, 1, 7, 2, 3, 5, 4, 6, 0};
        HibbardShellSort.hibbardShellSort(arr);
    }
}
